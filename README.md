# GitLab Accessibility

## What is this project for?

This project maintains a Docker image that GitLab uses to run accessibility testing.

## Getting Started

GitLab uses this project by specifying it as the image for GitLab runner to use in a CI job. That CI environment will have access to all the tools we install and use for accessibility testing, and our default usage pattern is maintained in [gitlab-accessibility.sh](https://gitlab.com/gitlab-org/ci-cd/accessibility/-/blob/master/gitlab-accessibility.sh).

If you're using GitLab CI, you can include [our template directly](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Verify/Accessibility.gitlab-ci.yml), or use it as a model for [writing your own configuration](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html#configure-accessibility-testing).

## Updates

Functional updates, individually or in groups, will be merged with [CHANGELOG](https://gitlab.com/gitlab-org/ci-cd/accessibility/-/blob/master/CHANGELOG.md) entries and released as a new version. **Breaking changes may be introduced in any version**. For this reason, we discourage use of referencing the `:latest` version of this image, and instead using a `:X.Y-gitlab.Z` docker tag.

Documentation-only updates will be merged to master without a version bump or new image release.

The GitLab project will consider all changes to this project for continuity before upgrading to a new version. For the smoothest GitLab Accessibility experience, we reccomend using this project via the [GitLab CI template](https://docs.gitlab.com/ee/user/project/merge_requests/accessibility_testing.html#accessibility-testing).

## Is it any good?

Yes.

If you disagree, please fill out [this form](https://gitlab.com/gitlab-org/gitlab/-/issues/new) and tell us why.
