FROM node:latest

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && apt-get update && apt-get install -y google-chrome-stable libxss1 && rm -rf /var/lib/apt/lists/*

RUN npm install -g pa11y@6.2.3 pa11y-reporter-html@2.0.0 pa11y-ci@3.0.1 puppeteer

COPY pa11y-configs/.pa11yci /pa11y-configs/.pa11yci
COPY pa11y-configs/pa11y.json /pa11y-configs/pa11y.json

COPY gitlab-accessibility.sh /gitlab-accessibility.sh
